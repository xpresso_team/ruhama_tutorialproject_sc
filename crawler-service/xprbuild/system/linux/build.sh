#! /bin/bash
## This script is used to build the project.
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user


# Build the dependencies
pip3 install -r ${ROOT_FOLDER}/requirements/requirements.txt
